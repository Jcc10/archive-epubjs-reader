
function preploads(){
  Book.book.on('Book.book:ready', function(href) {
    cssMods();
  });

  Book.book.on('renderer:chapterDisplayed', function(href) {
    cssMods();
  });
}

function cssMods() {
  var version = 5;
  var path = window.location.pathname.replace("reader.php", "");
  $("#viewer div iframe").contents().find("head").append('<link rel="stylesheet" href="' + path + '/css/Readers/ePub/customBook.css?' + version + '" type="text/css" />');
  borderChange();
  bgChange();
}

function borderChange() {
  var select = $("#viewer div iframe").contents().find("body");
  if ($("#pageBorders").is(":checked")){
     select.addClass("border");
  } else {
    select.removeClass("border");
  }
}

function mobileMode() {
  var select = $("#viewer div iframe").contents().find("body");
  if ($("#pageBorders").is(":checked")){
     select.css("margin","");
  } else {
    select.css("margin","0");
  }
}

function bgChange() {
  bookBG = $("#viewer div iframe").contents().find("body").css("background-color");
  currentBG = $("#main").css("background-color");
  if ((bookBG !== currentBG) && (bookBG !== "rgba(0, 0, 0, 0)")) {
    $("#main").css({"background-color": bookBG});
  }
}
